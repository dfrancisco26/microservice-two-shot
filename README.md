# Wardrobify

Team:

* David - Shoes
* Ashley - Hats

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

I'll make a shoe model that contains all the needed properties, with a foreign key relation to bins.
I'll create views for the GET, POST, and DELETE methods.
Using Router, we'll keep everything on one page.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

The Hats microservice uses polling from the wardrobe microservice to get the location within the wardrobe.
