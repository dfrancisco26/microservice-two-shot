import React, { useState, useEffect } from 'react';

function ShoeForm() {
    const [bin, setBin] = useState('');
    const [bins, setBins] = useState([]);
    const [manufacturer, setManufacturer] = useState('');
    const [model, setModel] = useState('');
    const [color, setColor] = useState('');
    const [image_url, setImage_url] = useState('');

    async function getBins() {
        const url = 'http://localhost:8100/api/bins/';

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            const bins = data.bins;
            setBins(bins);
        }
    }
    useEffect(() => {
        getBins();
    }, []);

    function handleBinChange(event) {
        setBin(event.target.value);
    }

    function handleManufacturerChange(event) {
        setManufacturer(event.target.value);
    }

    function handleModelChange(event) {
        setModel(event.target.value);
    }

    function handleColorChange(event) {
        setColor(event.target.value);
    }

    function handleImage_urlChange(event) {
        setImage_url(event.target.value);
    }

    async function handleSubmit(event) {
        event.preventDefault();
        console.log('handleSubmit called')
        const data = {
            manufacturer,
            model,
            color,
            image_url,
            bin,
        }
        console.log('Data:', data);

        const url = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            console.log('Shoe added to collection');
            const newShoe = await response.json();
            console.log(newShoe);
            setManufacturer('');
            setModel('');
            setColor('');
            setImage_url('');
            setBin('');
        }
        else {
            console.log('Error adding shoe to collection');
        }
    }

    
    return (
        <>
            <div>
                <h1>Add Shoe to Collection</h1>
                <form onSubmit={handleSubmit} id="create-shoe-form">
                    <div className="form-group">
                        <label htmlFor="manufacturer">Manufacturer</label>
                        <input value={manufacturer} onChange={handleManufacturerChange} required type="text" name="manufacturer" className="form-control" id="manufacturer" placeholder="Enter manufacturer" />
                        <label htmlFor="model">Model</label>
                        <input value={model} onChange={handleModelChange} required type="text" name="model" className="form-control" id="model" placeholder="Enter model" />
                        <label htmlFor="color">Color</label>
                        <input value={color} onChange={handleColorChange} required type="text" name="color" className="form-control" id="color" placeholder="Enter color" />
                        <label htmlFor="image_url">Image URL</label>
                        <input value={image_url} onChange={handleImage_urlChange} type="text" className="form-control" name="image_url" id="image_url" placeholder="Enter image URL" />
                        <label htmlFor="bin">Bin</label>
                        <select value={bin} onChange={handleBinChange} required name="bin" id="bin" className="form-select">
                            <option value="">Choose a bin</option>
                            {bins.map((bin, index) => {
                                return (
                                    <option key={index} value={bin.id}>{bin.id}</option>

                                )
                            })
                            }
                        </select>
                    </div>
                    <button className="btn btn-primary">Submit</button>
                </form>
        </div>
        </>
    )

}

export default ShoeForm;