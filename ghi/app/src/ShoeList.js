import React, { useEffect } from 'react'

function ShoeList() {
    const [shoes, setShoes] = React.useState([]);

    async function getShoes() {
        const response = await fetch('http://localhost:8080/api/shoes/');
        if (response.ok) {
            const data = await response.json();
            const shoes = data.shoes;
            setShoes(shoes);
            console.log(shoes);
        }
        else {
            console.log('Error retrieving shoes');
        }
    }
    useEffect(() => {
        getShoes();
    }, []);


    async function getShoes() {
    const response = await fetch('http://localhost:8080/api/shoes/');
    if (response.ok) {
        const data = await response.json();
        console.log('Data:', data); // Log the received data to check if it has the 'id' property
        const shoes = data.shoes;
        setShoes(shoes);
        console.log(shoes);
    } else {
        console.log('Error retrieving shoes');
    }
}

    async function deleteShoe(id) {
        console.log('Shoes Array:', shoes);
        console.log(id);
        const url = "http://localhost:8080/api/shoes/" + id;
        await fetch(url, { method: 'DELETE' });
        getShoes();
    
    }

    return (
        <>
        <div>
            <h1>Shoe List</h1>
                <div className="col">
                    {shoes.map((shoe, index) => {
                        return (
                            <div key={index} className="card mb-3">
                                <img src={shoe.image_url} className="card-img" alt="of shoe"/>
                                <div className="card-body">
                                    <p className="card-text">
                                        Manufacturer: {shoe.manufacturer}, Model: {shoe.model}, Color: {shoe.color}
                                    </p>
                                    <button type="button" onClick={() => deleteShoe(shoe.id)} className="btn btn-primary">Delete</button>
                                </div>
                            </div>
                        );
                    })}            
                </div>
        </div>
        </>
    )
}

export default ShoeList;