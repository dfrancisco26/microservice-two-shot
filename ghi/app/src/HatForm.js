import React, { useState, useEffect } from 'react';

function HatForm() {
    const [locations, setLocations] = useState([]);
    const [location, setLocation] = useState('');
    const [fabric, setFabric] = useState('');
    const [style, setStyle] = useState('');
    const [color, setColor] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');


    async function getLocations() {
        const url = "http://localhost:8100/api/locations/"

        const response = await fetch(url);
        // console.log(response);
        if (response.ok) {
            const data = await response.json();
            // console.log(data)
            setLocations(data.locations);
        }

    }
    useEffect(() => {
        getLocations();
      }, [])


    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            fabric,
            style,
            color,
            picture_url : pictureUrl,
            location,
        }
        // console.log(data);

        const hatUrl = "http://localhost:8090/api/hats/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json',
            },
          };
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            setFabric('');
            setColor('');
            setStyle('');
            setPictureUrl('');
            setLocation('');

        }

    }



    function handleChangeFabric(event) {
        const { value } = event.target;
        setFabric(value);
    }
    function handleChangeStyle(event) {
        const { value } = event.target;
        setStyle(value);
    }
    function handleChangeColor(event) {
        const { value } = event.target;
        setColor(value);
    }
    function handleChangePictureUrl(event) {
        const { value } = event.target;
        setPictureUrl(value);
    }
    function handleChangeLocation(event) {
        const { value } = event.target;
        setLocation(value);
     }



    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new hat</h1>
            <form onSubmit={handleSubmit} id="create-hat-form">
              <div className="form-floating mb-3">
                <input value={fabric} onChange={handleChangeFabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input value={style} onChange={handleChangeStyle} placeholder="Style" required type="text" name="style" id="style" className="form-control" />
                <label htmlFor="style">Style</label>
              </div>
              <div className="form-floating mb-3">
                <input value={color} onChange={handleChangeColor} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input value={pictureUrl} onChange={handleChangePictureUrl} placeholder="PictureURL" required type="text" name="picture_url" id="picture_url" className="form-control" />
                <label htmlFor="picture_url">PictureURL</label>
              </div>
              <div className="mb-3">
                <select value={location} onChange={handleChangeLocation} required name="location" id="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                    return (
                      <option key={location.id} value={location.id}>{location.id}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
      );
}

export default HatForm;
