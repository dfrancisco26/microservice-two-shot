import { useState, useEffect } from 'react';

function HatList() {
    const [hats, setHats] = useState([]);

    async function getHats() {
        const response = await fetch('http://localhost:8090/api/hats/');
        if (response.ok) {
            const data = await response.json();
            const hats = data.hats
            setHats(hats);
            // console.log(hats)
        } else {
            console.error('An error occured fetching the data')
        }
    }
    useEffect(() => {
        getHats();
    }, [])

    async function deleteHat(hatID) {
        console.log(hatID)
        const hatURL = 'http://localhost:8090/api/hats/' + hatID;

        await fetch(hatURL, {method: 'DELETE'});

    }


    return (
        <>
        <div className="col">
            {hats.map(hat => {
                return (
                    <div key={hat.id} className="card mb-3">
                        <img src={hat.picture_url} className="card-img" />
                        <div className="card-body">
                            <p className="card-text">
                                Color: {hat.color}, Fabric: {hat.fabric}, Style: {hat.style},
                            </p>
                            <p className="card-text">
                                Closet: {hat.location.closet_name}, in Section: {hat.location.section_number}, at Shelf: {hat.location.shelf_number}

                                id: {hat.id}
                            </p>
                        </div>
                        <button type="button" onClick={() => deleteHat(hat.id)}>Delete Hat</button>
                    </div>

                );
            })}
        </div>
        </>

    )
}

export default HatList;
