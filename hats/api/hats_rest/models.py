from django.db import models

# Create your models here.
class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100, null=True)
    section_number = models.PositiveSmallIntegerField(null=True)
    shelf_number = models.PositiveSmallIntegerField(null=True)


class Hat(models.Model):
    fabric = models.CharField(max_length=100)
    style = models.CharField(max_length=100, null=True)
    color = models.CharField(max_length=100, null=True)
    picture_url = models.URLField(max_length=200, null=True)
    location = models.ForeignKey(
        LocationVO,
        on_delete=models.CASCADE,
        null=True)


    class Meta:
        ordering = ("fabric", "style", "color", "picture_url", "location")
