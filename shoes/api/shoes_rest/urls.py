from django.urls import path
from .views import shoe_list, get_shoe

urlpatterns = [
    path("shoes/", shoe_list, name="shoe_list"),
    path("shoes/<int:id>/", get_shoe, name="get_shoe"),
]
