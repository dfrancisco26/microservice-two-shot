from django.db import models



class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100, null=True)
    bin_number = models.PositiveSmallIntegerField(null=True)
    bin_size = models.PositiveSmallIntegerField(null=True)


class Shoe(models.Model):

    manufacturer = models.CharField(max_length=100)
    model = models.CharField(max_length=100, null=True)
    color = models.CharField(max_length=100, null=True)
    image_url = models.URLField(max_length=1000, null=True)
    bin = models.ForeignKey(BinVO, on_delete=models.CASCADE, null=True)

    class Meta:
        ordering = ("manufacturer", "model", "color", "image_url", "bin")

