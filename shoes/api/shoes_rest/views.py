from .models import Shoe, BinVO
from django.http import JsonResponse
from common.json import ModelEncoder
import json
from django.views.decorators.http import require_http_methods
# Create your views here.


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["import_href", "id", "closet_name", "bin_number", "bin_size"]

class ShoeEncoder(ModelEncoder):
    model = Shoe
    properties = ["manufacturer", "id", "model", "color", "image_url", "bin"]
    encoders = {
        "bin": BinVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def shoe_list(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse({"shoes": shoes}, encoder=ShoeEncoder, safe=False)
    else:
        content = json.loads(request.body)

        try:
            bin = BinVO.objects.get(id=content["bin"])
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse({"error": "Bin does not exist"}, status=400)

        shoe = Shoe.objects.create(**content)
        return JsonResponse(shoe, encoder=ShoeEncoder, safe=False)


@require_http_methods(["GET", "DELETE"])
def get_shoe(request, id):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=id)
            return JsonResponse(shoe, encoder=ShoeEncoder, safe=False)
        except Shoe.DoesNotExist:
            return JsonResponse({"error": "Shoe does not exist"}, status=400)
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
